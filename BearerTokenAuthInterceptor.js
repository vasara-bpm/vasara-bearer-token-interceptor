// Inetrceptor to be used with VasaraBPM External task clients
// Adds a bearer token into the headers

class BearerTokenAuthInterceptor {
  /**
   * @throws Error
   */
  constructor(token) {
    if (!token) {
      throw new Error("Missing Authentication token");
    }

    /**
     * Bind member methods
     */
    this.getHeader = this.getHeader.bind(this);
    this.interceptor = this.interceptor.bind(this);

    this.header = this.getHeader(token);

    return this.interceptor;
  }

  getHeader(token) {
    return { Authorization: `Bearer ${token}` };
  }

  interceptor(config) {
    return { ...config, headers: { ...config.headers, ...this.header } };
  }
}

module.exports = BearerTokenAuthInterceptor;